[![pipeline status](https://gitlab.com/gaia-x/data-infrastructure-federation-services/tsa/golib/badges/main/pipeline.svg)](https://gitlab.com/gaia-x/data-infrastructure-federation-services/tsa/golib/-/commits/main)
[![coverage report](https://gitlab.com/gaia-x/data-infrastructure-federation-services/tsa/golib/badges/main/coverage.svg)](https://gitlab.com/gaia-x/data-infrastructure-federation-services/tsa/golib/-/commits/main)

# golib
This project has been migrated to Eclipse Foundation, and it can be found under https://gitlab.eclipse.org/eclipse/xfsc/

Go library with utility packages used in TSA backend services.

## GDPR

[GDPR](GDPR.md)

## Dependencies

[Dependencies](go.mod)

## License
<hr/>

[Apache 2.0 license](LICENSE)
